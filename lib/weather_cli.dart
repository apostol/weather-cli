import 'dart:io';
import 'dart:convert';

// https://openweathermap.org/weather-conditions#How-to-get-icon-URL

final Map<int, List<String>> emojis = {
  200: ['⛈️'], // thunderstorm with light rain
  201: ['⛈️'], // thunderstorm with rain
  202: ['⛈️'], // thunderstorm with heavy rain
  210: ['🌩️'], // light thunderstorm
  211: ['🌩️'], // thunderstorm
  212: ['🌩️'], // heavy thunderstorm
  221: ['🌩️'], // ragged thunderstorm
  230: ['🌩️'], // thunderstorm with light drizzle
  231: ['🌩️'], // thunderstorm with drizzle
  232: ['🌩️'], // thunderstorm with heavy drizzle
  300: ['🌧️'], // drizzle
  301: ['🌧️'],
  302: ['🌧️'],
  310: ['🌧️'],
  311: ['🌧️'],
  312: ['🌧️'],
  313: ['🌧️'],
  314: ['🌧️'],
  321: ['🌧️'],
  500: ['🌦️'], // rain
  501: ['🌦️'],
  502: ['🌧️'],
  503: ['🌧️'],
  504: ['⛈️'],
  511: ['🌨️'],
  520: ['🌧️'],
  521: ['🌧️'],
  522: ['🌧️'],
  531: ['🌧️'],
  600: ['🌨️'], // snow
  601: ['🌨️'],
  602: ['🌨️'],
  611: ['❄️'],
  612: ['❄️'],
  613: ['❄️'],
  615: ['🌨️⛆'],
  616: ['🌨️⛆'],
  620: ['🌨️⛆'],
  621: ['🌨️⛆'],
  622: ['🌨️⛆'],
  701: ['🌫️'], // mist
  711: ['😶‍🌫️'], // smoke
  721: ['🌫️'], // haze
  731: ['🌫️'], // dust
  741: ['🌁'], // fog
  751: ['🌫️'], // sand
  761: ['🌫️'], // dust
  762: ['🌋'], // ash
  771: ['💨'], // squall (gust of wind)
  781: ['🌪️'], // tornado
  800: ['☀️', '🌒'],
  801: ['🌤️'],
  802: ['⛅'],
  803: ['🌥️'],
  804: ['☁️'],
};

Map<String, dynamic> _applyDefault(Map<String, dynamic> o) => o;

Future<Map<String, dynamic>> getResponseFrom(String site,
    {Function apply = _applyDefault}) async {
  final url = Uri.parse(site);
  var httpClient = HttpClient();
  var request = await httpClient.getUrl(url);
  var response = await request.close();
  var objects = utf8.decoder.bind(response).transform(const JsonDecoder());
  Map<String, dynamic> result = {};
  try {
    await for (final o in objects) {
      if (result.isNotEmpty) {
        throw Exception('unexpected format of response');
      }
      result = apply(o);
    }
  } catch (e) {
    rethrow;
  } finally {
    httpClient.close();
  }
  return result;
}

/// Filter specified entries
Map<String, dynamic> filterKeys(List<String> filter, Map<String, dynamic> o) =>
    Map.fromEntries(o.entries.where((entry) => filter.contains(entry.key)));

/// Get full weather information
///
/// Extract all relevant entries from the weather
/// JSON information in the form of a flat map.
Map<String, dynamic> extractFull(Map<String, dynamic> o) {
  final mainEntry = o['main'];
  final weatherEntry = (o['weather'].first as Map<String, dynamic>);
  final cloudsEntry = o['clouds'] as Map<String, dynamic>;
  final windEntry = o['wind'] as Map<String, dynamic>;
  final sysEntry = o['sys'] as Map<String, dynamic>;
  Map<String, dynamic> result = {};
  result
    /* temp       -> double
       feels_like -> double
       temp_min   -> double
       temp_max   -> double
       pressure   -> int
       humidity   -> int */
    ..addEntries(mainEntry.entries)
    ..addAll(<String, dynamic>{
      'weather_id': weatherEntry['id'],
      'weather_main': weatherEntry['main'],
      'weather_description': weatherEntry['description'],
      'weather_icon': weatherEntry['icon'],
    })
    ..addAll(<String, dynamic>{
      'visibility': o['visibility'],
      'clouds_all': cloudsEntry['all'],
    })
    ..addAll(<String, dynamic>{
      'weather_main': weatherEntry['main'],
      'weather_description': weatherEntry['description'],
      'weather_icon': weatherEntry['icon'],
    })
    ..addAll(<String, dynamic>{
      'wind_speed': windEntry['speed'],
      'wind_deg': windEntry['deg'],
    })
    ..addAll(<String, dynamic>{
      'sys_sunrise': sysEntry['sunrise'],
      'sys_sunset': sysEntry['sunset'],
    });
  return result;
}

/// Get compact weather information
///
/// Extract the most relevant entries from the weather
/// JSON information in the form of a flat map.
Map<String, dynamic> extractCompact(Map<String, dynamic> o) {
  final mainEntry = o['main'];
  final weatherEntry = (o['weather'].first as Map<String, dynamic>);
  Map<String, dynamic> result = {};
  result
    /* temp       -> double
       feels_like -> double
       temp_min   -> double
       temp_max   -> double
       pressure   -> int
       humidity   -> int */
    ..addEntries(mainEntry.entries)
    ..addAll(<String, dynamic>{
      'weatherId': weatherEntry['id'],
      'description': weatherEntry['description'],
    });
  return result;
}
