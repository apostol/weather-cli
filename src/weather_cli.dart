import 'dart:io';
import 'package:args/args.dart';
import 'package:weather_cli/weather_cli.dart' as weather;

Future<Map<String, dynamic>> getLocation() async =>
    await weather.getResponseFrom('http://ip-api.com/json',
        apply: (o) => weather.filterKeys(['lat', 'lon', 'city', 'country'], o));

Future<Map<String, dynamic>> getWeatherFrom(String site) async =>
    await weather.getResponseFrom(site, apply: weather.extractCompact);

void main(List<String> args) async {
  exitCode = 0;
  bool isMetric = true;
  String city = '';
  String country = '';
  final parser = ArgParser()
    ..addSeparator('Usage: weather-cli [options]')
    ..addOption(
      'scale',
      abbr: 's',
      defaultsTo: 'Celsius',
      allowed: ['Celsius', 'Fahrenheit'],
      help: 'Temperature units',
      valueHelp: 'UNIT',
    )
    ..addOption(
      'city',
      abbr: 'c',
      defaultsTo: '',
      help: 'City (default: auto-determined)',
      valueHelp: 'London, etc',
    )
    ..addOption(
      'country',
      abbr: 'r',
      defaultsTo: '',
      help: 'Country (default: auto-determined)',
      valueHelp: 'uk, etc',
    )
    ..addFlag('help',
        abbr: 'h',
        defaultsTo: false,
        negatable: false,
        help: 'Display usage information');
  try {
    ArgResults argResults = parser.parse(args);
    if (argResults.rest.isNotEmpty) {
      throw ArgParserException('unexpected command line arguments.');
    }
    final bool showHelp = argResults['help'] as bool;
    if (showHelp) {
      print(parser.usage);
      return;
    }
    isMetric = argResults['scale'] == 'Celsius';
    city = argResults['city'];
    country = argResults['country'];
    if (country.isNotEmpty && city.isEmpty) {
      throw ArgParserException(
          'a city is required when the continent is specified.');
    }
  } on ArgParserException catch (e) {
    print('Argument specification: ${e.message}');
    print(parser.usage);
    exitCode = 2;
    return;
  } catch (e) {
    print(e);
    print(parser.usage);
    exitCode = 2;
    return;
  }

  final bool userLocation = city.isNotEmpty;
  Map<String, dynamic> location = {};
  if (userLocation) {
    location = {'city': city, 'country': country};
  } else {
    location = await getLocation();
    // print('lat=${location["lat"]}&lon=${location["lon"]}');
  }
  const apiKey = '315bfb21a64943c67a92e2da0022fdbe';
  final units = isMetric ? 'metric' : 'imperial';
  final weatherURL = userLocation
      ? 'https://api.openweathermap.org/data/2.5/weather?'
          'q=$city${country.isEmpty ? '' : ','}$country'
          '&APPID=$apiKey&units=$units'
      : 'http://api.openweathermap.org/data/2.5/weather?'
          'lat=${location["lat"]}&lon=${location["lon"]}'
          '&APPID=$apiKey&units=$units';
  final weatherInfo = await getWeatherFrom(weatherURL);
  final double temperature = weatherInfo['temp'];
  final double feelsLikeTemp = weatherInfo['feels_like'];
  final int weatherId = weatherInfo['weatherId'];
  final emoji = weather.emojis[weatherId]![0];
  final unit = isMetric ? '°C' : '°F';
  final String feelsLike =
      (temperature - feelsLikeTemp > .5 || temperature - feelsLikeTemp < -.5)
          ? ' (feels like $feelsLikeTemp$unit)'
          : '';
  country = userLocation ? country.toUpperCase() : location["country"];
  final sep = country.isEmpty ? '' : ', ';
  print('${location["city"]}$sep$country: '
      '${weatherInfo["description"]}, '
      '$temperature$unit $emoji'
      '$feelsLike');
}
