# weather-cli

Command line weather reporting tool

![](/demo.gif)

## Installation

```
snap install weather-cli
```

## Examples

Get current weather

```
$ weather-cli
```

Get current weather in Fahrenheit scale (default is Celcius)

```
$ weather-cli -f
```

Get current weather in city/country

```
$ weather-cli -c London -r UK
```

Display usahe information

```
$ weather-cli --help
```

## License

MIT

